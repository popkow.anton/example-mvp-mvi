package ru.alexkorrnd.tinkofffintechapp.data.api

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.alexkorrnd.tinkofffintechapp.data.DevelopersLifeService
import java.security.SecureRandom
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager


object RequestManager {

    private val retrofit = Retrofit.Builder()
        .client(createClient())
        .baseUrl("http://developerslife.ru/")
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    @Throws(Throwable::class)
    fun createClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .sslSocketFactory(createSslContext().socketFactory, createTrustManager())
            .build()
    }

    @Throws(Throwable::class)
    private fun createSslContext(): SSLContext {
        val sslContext: SSLContext = SSLContext.getInstance("TLS")
        sslContext.init(null, trustAll509(), SecureRandom())
        return sslContext
    }

    private fun createTrustManager(): X509TrustManager {
        return object : X509TrustManager {
            override fun checkClientTrusted(
                chain: Array<out java.security.cert.X509Certificate>?,
                authType: String?
            ) = Unit

            override fun checkServerTrusted(
                chain: Array<out java.security.cert.X509Certificate>?,
                authType: String?
            ) = Unit

            override fun getAcceptedIssuers(): Array<java.security.cert.X509Certificate> {
                return emptyArray()
            }
        }
    }

    private fun trustAll509(): Array<TrustManager> {
        return arrayOf(createTrustManager())
    }

    val service: DevelopersLifeService = retrofit.create(DevelopersLifeService::class.java)
}