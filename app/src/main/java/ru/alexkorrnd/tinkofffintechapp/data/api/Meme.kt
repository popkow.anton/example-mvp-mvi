package ru.alexkorrnd.tinkofffintechapp.data.api

import com.google.gson.annotations.SerializedName

data class Meme(
    val id: Int,
    val description: String,
    @SerializedName("gifURL")
    val gifUrl: String
)