package ru.alexkorrnd.tinkofffintechapp.data

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import ru.alexkorrnd.tinkofffintechapp.data.api.BaseResult
import ru.alexkorrnd.tinkofffintechapp.data.api.Meme

interface DevelopersLifeService {
    @GET("latest/{pageNumber}?json=true")
    fun loadMemes(@Path("pageNumber") pageNumber: Int): Single<BaseResult<List<Meme>>>
}