package ru.alexkorrnd.tinkofffintechapp.di

import ru.alexkorrnd.tinkofffintechapp.data.MemesRepository
import ru.alexkorrnd.tinkofffintechapp.data.api.RequestManager
import ru.alexkorrnd.tinkofffintechapp.domain.LoadMemes
import ru.alexkorrnd.tinkofffintechapp.presentation.meme.MemesPresenter

class GlobalDI private constructor() {

    val repository by lazy { MemesRepository(RequestManager.service) }

    val loadMemes by lazy { LoadMemes(repository) }

    val presenter by lazy {
        MemesPresenter(
            loadMemes = loadMemes
        )
    }

    companion object {

        lateinit var INSTANCE: GlobalDI

        fun init() {
            INSTANCE = GlobalDI()
        }
    }
}