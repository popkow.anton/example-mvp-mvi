package ru.alexkorrnd.tinkofffintechapp.presentation.meme.adapter

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateLayoutContainer
import kotlinx.android.synthetic.main.item_meme.*
import ru.alexkorrnd.tinkofffintechapp.R
import ru.alexkorrnd.tinkofffintechapp.data.api.Meme

internal val memeDelegate = adapterDelegateLayoutContainer<Meme, Any>(
    layout = R.layout.item_meme,
    on = { item, _, _ -> item is Meme },
    block = {
        bind {
            Glide.with(memeView)
                .asGif()
                .load(item.gifUrl)
                .centerCrop()
                .placeholder(context.createProgressPlaceholderDrawable())
                .into(memeView)
                .waitForLayout()
            memeText.text = item.description
        }
    }
)

private fun Context.createProgressPlaceholderDrawable(): Drawable {
    return CircularProgressDrawable(this).apply {
        strokeWidth = resources.getDimension(R.dimen.item_meme_progress_width)
        centerRadius = resources.getDimension(R.dimen.item_meme_progress_radius)
        setColorSchemeColors(
            ContextCompat.getColor(
                this@createProgressPlaceholderDrawable,
                R.color.colorAccent
            )
        )
        start()
    }
}
