package ru.alexkorrnd.tinkofffintechapp.presentation.meme

sealed class UiEffect {

    class NextPageLoadError(val error: Throwable): UiEffect()

}