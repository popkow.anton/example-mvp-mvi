package ru.alexkorrnd.tinkofffintechapp.presentation.meme

import android.util.Log
import com.freeletics.rxredux.SideEffect
import com.freeletics.rxredux.reduxStore
import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import ru.alexkorrnd.tinkofffintechapp.domain.LoadMemes
import ru.alexkorrnd.tinkofffintechapp.presentation.base.mvp.presenter.RxPresenter

private typealias MemeSideEffect = SideEffect<State, out Action>

class MemesPresenter(
    private val loadMemes: LoadMemes
) : RxPresenter<MemeView>(MemeView::class.java) {

    private val inputRelay: Relay<Action> = PublishRelay.create()
    private val uiEffectsRelay = PublishRelay.create<UiEffect>()

    val input: Consumer<Action> get() = inputRelay
    private val uiEffectsInput: Observable<UiEffect> get() = uiEffectsRelay

    private val state: Observable<State> = inputRelay.reduxStore(
        initialState = State(),
        sideEffects = listOf(loadFirstPage(), loadNextPage()),
        reducer = State::reduce
    )

    override fun attachView(view: MemeView) {
        super.attachView(view)
        state.observeOn(AndroidSchedulers.mainThread())
            .subscribe(view::render)
            .disposeOnFinish()
        uiEffectsInput.observeOn(AndroidSchedulers.mainThread())
            .subscribe(view::handleUiEffect)
            .disposeOnFinish()
    }

    private fun loadFirstPage(): MemeSideEffect {
        return { actions, state ->
            actions.ofType(Action.LoadFirstPage::class.java)
                .switchMap {
                    loadPage(state().pageNumber)
                        .onErrorReturn { error -> Action.ErrorLoadFirstPage(error) }
                }
        }
    }

    private fun loadNextPage(): MemeSideEffect {
        return { actions, state ->
            actions.ofType(Action.LoadNextPage::class.java)
                .distinctUntilChanged()
                .switchMap {
                    loadPage(state().pageNumber)
                        .onErrorReturn { error ->
                            uiEffectsRelay.accept(UiEffect.NextPageLoadError(error))
                            Action.StopLoadingNextPage
                        }
                }
        }
    }

    private fun loadPage(pageNumber: Int): Observable<Action> {
        return loadMemes.execute(pageNumber)
            .subscribeOn(Schedulers.io())
            .toObservable()
            .map { items -> Action.PageLoaded(items = items) }
    }

}