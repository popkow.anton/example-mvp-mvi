package ru.alexkorrnd.tinkofffintechapp.presentation.meme.adapter

import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateLayoutContainer
import ru.alexkorrnd.tinkofffintechapp.R

internal val nextPageLoaderDelegate =
    adapterDelegateLayoutContainer<NextPageLoader, Any>(
        layout = R.layout.item_next_page_loader,
        on = { item, _, _ -> item is NextPageLoader },
        block = {}
    )