package ru.alexkorrnd.tinkofffintechapp.domain

import io.reactivex.Single
import ru.alexkorrnd.tinkofffintechapp.data.MemesRepository
import ru.alexkorrnd.tinkofffintechapp.data.api.Meme

class LoadMemes(
    private val memesRepository: MemesRepository
) {

    fun execute(page: Int): Single<List<Meme>> {
        return memesRepository.loadMemes(page)
    }
}