package ru.alexkorrnd.tinkofffintechapp

import android.app.Application
import ru.alexkorrnd.tinkofffintechapp.di.GlobalDI

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        GlobalDI.init()
    }

}